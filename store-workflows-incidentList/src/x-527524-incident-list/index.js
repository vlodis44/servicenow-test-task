import {createCustomElement} from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import '@servicenow/now-template-card';
import { createHttpEffect } from '@servicenow/ui-effect-http';
import { FETCH_FAILED, FETCH_SUCCEEDED, FETCH_LIST } from './constants';

const fetchIncidentsEffect = createHttpEffect('api/now/table/incident?sysparm_display_value=true', {
	method: 'GET',
	headers: {},
	queryParams: ['sysparm_display_value=true'],
	pathParams: [],
	dataParam: 'data',
	successActionType: FETCH_SUCCEEDED,
	errorActionType: FETCH_FAILED
});

const handleFetchIncidentsSucceeded = ({action, updateState}) => {
	updateState({
		path: 'list',
		value: action.payload.result,
		operation: 'concat'
	})
};

const handleFetchIncidentsFailed = () => alert('Data fetch failed!');

const view = (state, {dispatch}) => {
	let incidentList = [];

	if (!state.list.length) {
		dispatch(FETCH_LIST);
	} else {
		incidentList = state.list.map(incident => (
			<now-template-card-assist
				className="now-template-card"
				tagline={{icon: "tree-view-long-outline", label: incident.sys_class_name}}
				actions={[
					{id: 'share', label: 'Copy URL'},
					{id: 'close', label: 'Mark Complete'}
				]}
				heading={{label: incident.short_description}}
				content={[
					{label: 'Number', value: {type: 'string', value: incident.number}},
					{label: 'State', value: {type: 'string', value: incident.state}},
					{label: 'Assignment Group', value: {type: 'string', value: incident.assignment_group.display_value}},
					{label: 'Assigned To', value: {type: 'string', value: incident.assigned_to.display_value}}
				]}
				footer-content={{
					label: 'Updated',
					value: incident.sys_updated_on
				}}
			/>
		))
	}

	return (
		<div className="container">
			{incidentList.length ? incidentList : 'Please wait, data is loading...'}
		</div>
	);
};

createCustomElement('x-527524-incident-list', {
	renderer: {type: snabbdom},
	view,
	styles,
	initialState: {
		list: []
	},
	actionHandlers: {
		[FETCH_LIST]: fetchIncidentsEffect,
		[FETCH_SUCCEEDED]: handleFetchIncidentsSucceeded,
		[FETCH_FAILED]: handleFetchIncidentsFailed
	}
});
