import {createCustomElement} from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import "@servicenow/now-template-card";

const mockData = [
	{
		label: 'my PDF docs are all locked from editing',
		number: 'INC0000038',
		state: 'Closed',
		assignmentGroup: 'Service Desk',
		assignedTo: 'Luke Wilson',
		updated: '2020-05-08 17:36:44'
	},
	{
		label: 'Hangs when trying to print VISIO document',
		number: 'INC0000006',
		state: 'Closed',
		assignmentGroup: 'Software',
		assignedTo: 'Howard Johnson',
		updated: '2020-05-01 16:08:05'
	},
	{
		label: 'Printer in my office is out of toner',
		number: 'INC0000008',
		state: 'Closed',
		assignmentGroup: 'Hardware',
		assignedTo: 'ITIL User',
		updated: '2020-05-09 16:08:39'
	},
	{
		label: 'Can\'t read email',
		number: 'INC0000001',
		state: 'Closed',
		assignmentGroup: 'Service Desk',
		assignedTo: 'Charlie Whitherspoon',
		updated: '2020-05-01 16:09:51'
	}
]

const view = (state, {updateState}) => {
	return (
		<div className="container">
			{mockData.map(incident => (
				<now-template-card-assist
					className="now-template-card"
					tagline={{icon: "tree-view-long-outline", label: 'Incident'}}
					actions={[
						{id: 'share', label: 'Copy URL'},
						{id: 'close', label: 'Mark Complete'}
					]}
					heading={{label: incident.label}}
					content={[
						{label: 'Number', value: {type: 'string', value: incident.number}},
						{label: 'State', value: {type: 'string', value: incident.state}},
						{label: 'Assignment Group', value: {type: 'string', value: incident.assignmentGroup}},
						{label: 'Assigned To', value: {type: 'string', value: incident.assignedTo}}
					]}
					footer-content={{
						label: 'Updated',
						value: incident.updated
					}}
				/>
			))}
		</div>
	);
};

createCustomElement('x-527524-card-list', {
	renderer: {type: snabbdom},
	view,
	styles
});
