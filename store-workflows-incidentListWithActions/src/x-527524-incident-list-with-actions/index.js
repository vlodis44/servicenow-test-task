import { createCustomElement, actionTypes } from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import '@servicenow/now-template-card';
import '@servicenow/now-modal';
import '@servicenow/now-label-value'
import { createHttpEffect } from '@servicenow/ui-effect-http';
import { FETCH_LIST, FETCH_FAILED, FETCH_SUCCEEDED, DELETE_TICKET, DELETE_SUCCEEDED, DELETE_FAILED } from './constants';

const { COMPONENT_BOOTSTRAPPED } = actionTypes;

const fetchIncidentsEffect = createHttpEffect('api/now/table/incident?sysparm_display_value=true', {
	method: 'GET',
	headers: {},
	queryParams: ['sysparm_display_value=true'],
	pathParams: [],
	dataParam: 'data',
	successActionType: FETCH_SUCCEEDED,
	errorActionType: FETCH_FAILED
});

const handleFetchIncidentsSucceeded = ({action, updateState}) => {
	updateState({
		path: 'list',
		value: action.payload.result,
		operation: 'concat'
	});

	updateState({
		path: 'mappedList',
		value: action.payload.result.map((incident, index) => {
			window[index + '_ticketRef'] = snabbdom.createRef();
			return (
				<now-template-card-assist
					ref={window[index + '_ticketRef']}
					key={incident.number}
					className="now-template-card"
					tagline={{icon: 'tree-view-long-outline', label: incident.sys_class_name}}
					actions={[
						{id: `${index}_open`, label: 'Open record'},
						{id: `${index}_delete`, label: 'Delete'}
					]}
					heading={{label: incident.short_description}}
					content={[
						{label: 'Number', value: {type: 'string', value: incident.number}},
						{label: 'State', value: {type: 'string', value: incident.state}},
						{
							label: 'Assignment Group',
							value: {type: 'string', value: incident.assignment_group.display_value}
						},
						{label: 'Assigned To', value: {type: 'string', value: incident.assigned_to.display_value}}
					]}
					footer-content={{
						label: 'Updated',
						value: incident.sys_updated_on
					}}
					data-sys_id={incident.sys_id}
				/>)
		}),
		operation: 'concat'
	})
};

const handleFetchIncidentsFailed = () => alert('Data fetch failed!');

const deleteTicketEffect = createHttpEffect('api/now/table/incident/:sys_id', {
	method: 'DELETE',
	headers: {},
	queryParams: [],
	pathParams: ['sys_id'],
	dataParam: 'data',
	successActionType: DELETE_SUCCEEDED,
	errorActionType: DELETE_FAILED
});

const handleDeleteSucceeded = ({state, action, updateState}) => {
	alert('Deleted!');

	const ticketId = action.meta.request.updatedUrl.replace('api/now/table/incident/', '');
	const ticketIndex = state.list.findIndex(ticket => ticket.sys_id === ticketId);

	updateState({
		path: 'list',
		operation: 'splice',
		start: ticketIndex,
		deleteCount: 1
	});
	updateState({
		path: 'mappedList',
		operation: 'splice',
		start: ticketIndex,
		deleteCount: 1
	})
};

const handleDeleteFailed = () => alert('Can\'t delete ticket!');

const onDropdownItemClick = ({dispatch, state, action, updateState}) => {
	const labelId = action.payload.item.id;
	const underscoreIndex = labelId.indexOf('_');
	const ticketIndex = labelId.slice(0, underscoreIndex);
	const actionName = labelId.slice(underscoreIndex + 1);
	const ticketRef = window[ticketIndex + '_ticketRef'];
	const ticketSysId = ticketRef.current.dataset.sys_id;

	switch (actionName) {
		case 'open': {
			updateState({
				path: 'modal',
				value: {
					isOpened: true,
					data: state.list.find(ticket => ticket.sys_id === ticketSysId)
				},
				operation: 'set'
			})
			break;
		}
		case 'delete': {
			if (!confirm('Confirm ticket delete')) { break; }
			dispatch(DELETE_TICKET, { sys_id: ticketSysId })
			break;
		}
	}

};

const onModalDeleteClick = ({state, dispatch}) => {
	if (!confirm('Confirm ticket delete')) { return; }
	dispatch(DELETE_TICKET, { sys_id: state.modal.data.sys_id });
	dispatch('NOW_MODAL#OPENED_SET');
};

const onModalClose = ({updateState}) => {
	updateState({
		path: 'modal',
		value: {
			isOpened: false,
			data: {}
		},
		operation: 'set'
	})
}

const view = (state) => {

	return (
		<div className="container">
			<now-modal
				opened={state.modal.isOpened}
				size='md'
				content={state.modal.isOpened ? (
					<now-label-value-tabbed
						items={[
							{
								label: 'Number',
								value: {type: 'string', value: state.modal.data.number}
							},
							{
								label: 'State',
								value: {type: 'string', value: state.modal.data.state}
							},
							{
								label: 'Opened at',
								value: {type: 'string', value: state.modal.data.opened_at}
							},
							{
								label: 'Short description',
								value: {type: 'string', value: state.modal.data.short_description}
							},
							{
								label: 'Assignment group',
								value: {type: 'string', value: state.modal.data.assignment_group.display_value}
							},
							{
								label: 'Assigned to',
								value: {type: 'string', value: state.modal.data.assigned_to.display_value}
							}
						]}
					/>
				)
				: null}
				footer-actions='[
					{
						"variant": "primary",
						"label": "Delete"
					}
				]'
			/>
			{state.mappedList[0] ? state.mappedList : 'Please wait, data is loading...'}
		</div>
	);
};

createCustomElement('x-527524-incident-list-with-actions', {
	renderer: {type: snabbdom},
	view,
	styles,
	initialState: {
		list: [],
		mappedList: [],
		modal: {
			isOpened: false,
			data: {}
		}
	},
	actionHandlers: {
		[COMPONENT_BOOTSTRAPPED]: ({dispatch}) => { dispatch(FETCH_LIST) },
		[FETCH_LIST]: fetchIncidentsEffect,
		[FETCH_SUCCEEDED]: handleFetchIncidentsSucceeded,
		[FETCH_FAILED]: handleFetchIncidentsFailed,
		[DELETE_TICKET]: deleteTicketEffect,
		[DELETE_SUCCEEDED]: handleDeleteSucceeded,
		[DELETE_FAILED]: handleDeleteFailed,
		'NOW_DROPDOWN_PANEL#ITEM_CLICKED' : onDropdownItemClick,
		'NOW_MODAL#OPENED_SET': onModalClose,
		'NOW_MODAL#FOOTER_ACTION_CLICKED': onModalDeleteClick
	}
});
