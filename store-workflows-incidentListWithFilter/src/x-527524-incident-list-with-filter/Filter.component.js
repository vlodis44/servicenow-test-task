import { createCustomElement } from '@servicenow/ui-core';
import snabbdom from '@servicenow/ui-renderer-snabbdom';
import styles from './styles.scss';
import '@servicenow/now-card';

const onInputChange = (updateState, path) => event => {
	updateState({
		operation: 'set',
		path: 'filter.' + path,
		value: event.target.value
	});
}

const onFilterButtonClick = (filterObj, callback) => () => {
	callback(filterObj);
}

const view = (state, { updateState }) => {
	const { properties: { callback } } = state;

	return (
		<now-card size="lg" interaction="select">
			<now-card-header
				tagline={{"label":"Filter","icon":"filter-outline"}}
			/>
			<div className="filter-option progress-input">
				<label>
					Progress:&nbsp;
					<select name="progress" size="1" onchange={onInputChange(updateState, 'progress')}>
						<option value="all">All</option>
						<option value="closed">Closed</option>
						<option value="in progress">In Progress</option>
						<option value="on hold">On Hold</option>
						<option value="new">New</option>
					</select>
				</label>
			</div>
			<div className="filter-option description-input">
				<label>
					Description:&nbsp;
					<input type="text" value={state.filter.description}
						   onchange={onInputChange(updateState, 'description')}
					/>
				</label>
			</div>
			<div className="filter-option number-input">
				<label>
					Number:&nbsp;
					<input type="text" value={state.filter.number}
						   onchange={onInputChange(updateState, 'number')}
					/>
				</label>
			</div>
			<div className="filter-option assignment-group-input">
				<label>
					Assignment group:&nbsp;
					<input type="text" value={state.filter.assignmentGroup}
						   onchange={onInputChange(updateState, 'assignmentGroup')}
					/>
				</label>
			</div>
			<div className="filter-option assigned-to-input">
				<label>
					Assigned person:&nbsp;
					<input type="text" value={state.filter.assignedPerson}
						   onchange={onInputChange(updateState, 'assignedPerson')}
					/>
				</label>
			</div>
			<now-card-actions className="filter-button" items={[{label: 'Filter', variant: 'primary'}]}
				onclick={onFilterButtonClick(state.filter, callback)}
			/>
		</now-card>
	);
};

createCustomElement('x-527524-filter', {
	renderer: {type: snabbdom},
	view,
	styles,
	initialState: {
		filter: {
			progress: 'all',
			description: '',
			number: '',
			assignmentGroup: '',
			assignedPerson: ''
		}
	},
	properties: {
		callback: { default: () => {} }
	}
});
